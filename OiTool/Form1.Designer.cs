﻿namespace OiTool
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslCompany = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtChipDiagonal = new System.Windows.Forms.TextBox();
            this.txtChipHeight = new System.Windows.Forms.TextBox();
            this.txtChipWidth = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbChipSize = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtObjectDistance = new System.Windows.Forms.TextBox();
            this.txtObjectHeight = new System.Windows.Forms.TextBox();
            this.txtObjectWidth = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnCompute = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtFocalDistance = new System.Windows.Forms.TextBox();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtProcess = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.btnCompute1 = new System.Windows.Forms.Button();
            this.txtObjectWidth1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtObjectHeight1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtChipDiagonal1 = new System.Windows.Forms.TextBox();
            this.txtChipHeight1 = new System.Windows.Forms.TextBox();
            this.txtChipWidth1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbChipSize1 = new System.Windows.Forms.ComboBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnClear1 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtFocalDistance1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtObjectDistance1 = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtChipDiagonal2 = new System.Windows.Forms.TextBox();
            this.txtChipHeight2 = new System.Windows.Forms.TextBox();
            this.txtChipWidth2 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.cmbChipSize2 = new System.Windows.Forms.ComboBox();
            this.txtMaxResolution = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMTF = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btnCompute3 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.btnCompute2 = new System.Windows.Forms.Button();
            this.txtMagnification = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtChipLength = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtObjectLength = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslCompany});
            this.statusStrip1.Location = new System.Drawing.Point(0, 483);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(536, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslCompany
            // 
            this.tsslCompany.IsLink = true;
            this.tsslCompany.Name = "tsslCompany";
            this.tsslCompany.Size = new System.Drawing.Size(152, 17);
            this.tsslCompany.Text = "苏州光图智能科技有限公司";
            this.tsslCompany.Click += new System.EventHandler(this.tsslCompany_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtChipDiagonal);
            this.groupBox2.Controls.Add(this.txtChipHeight);
            this.groupBox2.Controls.Add(this.txtChipWidth);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.cmbChipSize);
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(158, 232);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "芯片尺寸";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(11, 204);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(137, 12);
            this.label15.TabIndex = 3;
            this.label15.Text = "芯片尺寸不对请手动改正";
            // 
            // txtChipDiagonal
            // 
            this.txtChipDiagonal.Location = new System.Drawing.Point(20, 172);
            this.txtChipDiagonal.Name = "txtChipDiagonal";
            this.txtChipDiagonal.ReadOnly = true;
            this.txtChipDiagonal.Size = new System.Drawing.Size(121, 21);
            this.txtChipDiagonal.TabIndex = 2;
            // 
            // txtChipHeight
            // 
            this.txtChipHeight.Location = new System.Drawing.Point(20, 128);
            this.txtChipHeight.Name = "txtChipHeight";
            this.txtChipHeight.Size = new System.Drawing.Size(121, 21);
            this.txtChipHeight.TabIndex = 2;
            // 
            // txtChipWidth
            // 
            this.txtChipWidth.Location = new System.Drawing.Point(20, 84);
            this.txtChipWidth.Name = "txtChipWidth";
            this.txtChipWidth.Size = new System.Drawing.Size(121, 21);
            this.txtChipWidth.TabIndex = 2;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(18, 157);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 12);
            this.label13.TabIndex = 1;
            this.label13.Text = "对角线(mm)：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 113);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "芯片高(mm)：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "芯片宽(mm)：";
            // 
            // cmbChipSize
            // 
            this.cmbChipSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChipSize.FormattingEnabled = true;
            this.cmbChipSize.Items.AddRange(new object[] {
            "1/4英寸",
            "1/3英寸",
            "1/2.5英寸",
            "1/2.3英寸",
            "1/2英寸",
            "1/1.8英寸",
            "2/3英寸",
            "1英寸",
            "1.1英寸",
            "35mm film"});
            this.cmbChipSize.Location = new System.Drawing.Point(20, 39);
            this.cmbChipSize.Name = "cmbChipSize";
            this.cmbChipSize.Size = new System.Drawing.Size(121, 20);
            this.cmbChipSize.TabIndex = 0;
            this.cmbChipSize.SelectedIndexChanged += new System.EventHandler(this.cmbChipSize_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnClear);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtObjectDistance);
            this.groupBox3.Controls.Add(this.txtObjectHeight);
            this.groupBox3.Controls.Add(this.txtObjectWidth);
            this.groupBox3.Location = new System.Drawing.Point(170, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(222, 126);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "测算参数";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(96, 97);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 3;
            this.btnClear.Text = "清空";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 76);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 12);
            this.label5.TabIndex = 1;
            this.label5.Text = "物距(mm)：";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 1;
            this.label4.Text = "物高(mm)：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "物宽(mm)：";
            // 
            // txtObjectDistance
            // 
            this.txtObjectDistance.Location = new System.Drawing.Point(91, 73);
            this.txtObjectDistance.Name = "txtObjectDistance";
            this.txtObjectDistance.Size = new System.Drawing.Size(88, 21);
            this.txtObjectDistance.TabIndex = 2;
            // 
            // txtObjectHeight
            // 
            this.txtObjectHeight.Location = new System.Drawing.Point(91, 46);
            this.txtObjectHeight.Name = "txtObjectHeight";
            this.txtObjectHeight.Size = new System.Drawing.Size(88, 21);
            this.txtObjectHeight.TabIndex = 2;
            // 
            // txtObjectWidth
            // 
            this.txtObjectWidth.Location = new System.Drawing.Point(91, 19);
            this.txtObjectWidth.Name = "txtObjectWidth";
            this.txtObjectWidth.Size = new System.Drawing.Size(88, 21);
            this.txtObjectWidth.TabIndex = 2;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btnCompute);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.txtFocalDistance);
            this.groupBox4.Location = new System.Drawing.Point(170, 140);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(222, 98);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "计算结果";
            // 
            // btnCompute
            // 
            this.btnCompute.Location = new System.Drawing.Point(96, 20);
            this.btnCompute.Name = "btnCompute";
            this.btnCompute.Size = new System.Drawing.Size(75, 23);
            this.btnCompute.TabIndex = 3;
            this.btnCompute.Text = "计算";
            this.btnCompute.UseVisualStyleBackColor = true;
            this.btnCompute.Click += new System.EventHandler(this.btnCompute_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 64);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "焦距(mm)：";
            // 
            // txtFocalDistance
            // 
            this.txtFocalDistance.Location = new System.Drawing.Point(91, 61);
            this.txtFocalDistance.Name = "txtFocalDistance";
            this.txtFocalDistance.Size = new System.Drawing.Size(88, 21);
            this.txtFocalDistance.TabIndex = 2;
            // 
            // tabControl2
            // 
            this.tabControl2.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            this.tabControl2.Controls.Add(this.tabPage1);
            this.tabControl2.Controls.Add(this.tabPage2);
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 64);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(536, 419);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Location = new System.Drawing.Point(4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(528, 393);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "焦距计算";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txtProcess);
            this.groupBox1.Location = new System.Drawing.Point(6, 244);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(386, 143);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "计算过程";
            // 
            // txtProcess
            // 
            this.txtProcess.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtProcess.Location = new System.Drawing.Point(3, 17);
            this.txtProcess.Multiline = true;
            this.txtProcess.Name = "txtProcess";
            this.txtProcess.ReadOnly = true;
            this.txtProcess.Size = new System.Drawing.Size(380, 123);
            this.txtProcess.TabIndex = 2;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox5);
            this.tabPage2.Controls.Add(this.groupBox6);
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Location = new System.Drawing.Point(4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(528, 393);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "物距/视场计算";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnCompute1);
            this.groupBox5.Controls.Add(this.txtObjectWidth1);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.txtObjectHeight1);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Location = new System.Drawing.Point(170, 125);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(222, 113);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "计算结果";
            // 
            // btnCompute1
            // 
            this.btnCompute1.Location = new System.Drawing.Point(96, 20);
            this.btnCompute1.Name = "btnCompute1";
            this.btnCompute1.Size = new System.Drawing.Size(75, 23);
            this.btnCompute1.TabIndex = 3;
            this.btnCompute1.Text = "计算";
            this.btnCompute1.UseVisualStyleBackColor = true;
            this.btnCompute1.Click += new System.EventHandler(this.btnCompute1_Click);
            // 
            // txtObjectWidth1
            // 
            this.txtObjectWidth1.Location = new System.Drawing.Point(91, 49);
            this.txtObjectWidth1.Name = "txtObjectWidth1";
            this.txtObjectWidth1.Size = new System.Drawing.Size(88, 21);
            this.txtObjectWidth1.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(20, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(65, 12);
            this.label11.TabIndex = 1;
            this.label11.Text = "物高(mm)：";
            // 
            // txtObjectHeight1
            // 
            this.txtObjectHeight1.Location = new System.Drawing.Point(91, 76);
            this.txtObjectHeight1.Name = "txtObjectHeight1";
            this.txtObjectHeight1.Size = new System.Drawing.Size(88, 21);
            this.txtObjectHeight1.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(20, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 1;
            this.label12.Text = "物宽(mm)：";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtChipDiagonal1);
            this.groupBox6.Controls.Add(this.txtChipHeight1);
            this.groupBox6.Controls.Add(this.txtChipWidth1);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.cmbChipSize1);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(158, 232);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "芯片尺寸";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(11, 204);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 12);
            this.label16.TabIndex = 10;
            this.label16.Text = "芯片尺寸不对请手动改正";
            // 
            // txtChipDiagonal1
            // 
            this.txtChipDiagonal1.Location = new System.Drawing.Point(20, 172);
            this.txtChipDiagonal1.Name = "txtChipDiagonal1";
            this.txtChipDiagonal1.ReadOnly = true;
            this.txtChipDiagonal1.Size = new System.Drawing.Size(121, 21);
            this.txtChipDiagonal1.TabIndex = 7;
            // 
            // txtChipHeight1
            // 
            this.txtChipHeight1.Location = new System.Drawing.Point(20, 128);
            this.txtChipHeight1.Name = "txtChipHeight1";
            this.txtChipHeight1.Size = new System.Drawing.Size(121, 21);
            this.txtChipHeight1.TabIndex = 8;
            // 
            // txtChipWidth1
            // 
            this.txtChipWidth1.Location = new System.Drawing.Point(20, 84);
            this.txtChipWidth1.Name = "txtChipWidth1";
            this.txtChipWidth1.Size = new System.Drawing.Size(121, 21);
            this.txtChipWidth1.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 12);
            this.label8.TabIndex = 4;
            this.label8.Text = "对角线(mm)：";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 113);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 12);
            this.label9.TabIndex = 5;
            this.label9.Text = "芯片高(mm)：";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(18, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 12);
            this.label14.TabIndex = 6;
            this.label14.Text = "芯片宽(mm)：";
            // 
            // cmbChipSize1
            // 
            this.cmbChipSize1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChipSize1.FormattingEnabled = true;
            this.cmbChipSize1.Items.AddRange(new object[] {
            "1/4英寸",
            "1/3英寸",
            "1/2.5英寸",
            "1/2.3英寸",
            "1/2英寸",
            "1/1.8英寸",
            "2/3英寸",
            "1英寸",
            "1.1英寸",
            "35mm film"});
            this.cmbChipSize1.Location = new System.Drawing.Point(20, 39);
            this.cmbChipSize1.Name = "cmbChipSize1";
            this.cmbChipSize1.Size = new System.Drawing.Size(121, 20);
            this.cmbChipSize1.TabIndex = 3;
            this.cmbChipSize1.SelectedIndexChanged += new System.EventHandler(this.cmbChipSize1_SelectedIndexChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnClear1);
            this.groupBox7.Controls.Add(this.label7);
            this.groupBox7.Controls.Add(this.txtFocalDistance1);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.txtObjectDistance1);
            this.groupBox7.Location = new System.Drawing.Point(170, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(222, 113);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "测算参数";
            // 
            // btnClear1
            // 
            this.btnClear1.Location = new System.Drawing.Point(96, 82);
            this.btnClear1.Name = "btnClear1";
            this.btnClear1.Size = new System.Drawing.Size(75, 23);
            this.btnClear1.TabIndex = 3;
            this.btnClear1.Text = "清空";
            this.btnClear1.UseVisualStyleBackColor = true;
            this.btnClear1.Click += new System.EventHandler(this.btnClear1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 1;
            this.label7.Text = "焦距(mm)：";
            // 
            // txtFocalDistance1
            // 
            this.txtFocalDistance1.Location = new System.Drawing.Point(91, 27);
            this.txtFocalDistance1.Name = "txtFocalDistance1";
            this.txtFocalDistance1.Size = new System.Drawing.Size(88, 21);
            this.txtFocalDistance1.TabIndex = 2;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(20, 58);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 1;
            this.label10.Text = "物距(mm)：";
            // 
            // txtObjectDistance1
            // 
            this.txtObjectDistance1.Location = new System.Drawing.Point(91, 55);
            this.txtObjectDistance1.Name = "txtObjectDistance1";
            this.txtObjectDistance1.Size = new System.Drawing.Size(88, 21);
            this.txtObjectDistance1.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox9);
            this.tabPage3.Controls.Add(this.groupBox8);
            this.tabPage3.Location = new System.Drawing.Point(4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(528, 393);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "远心镜头";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.groupBox10);
            this.groupBox9.Controls.Add(this.txtMaxResolution);
            this.groupBox9.Controls.Add(this.label21);
            this.groupBox9.Controls.Add(this.txtMTF);
            this.groupBox9.Controls.Add(this.label24);
            this.groupBox9.Controls.Add(this.btnCompute3);
            this.groupBox9.Location = new System.Drawing.Point(187, 12);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(319, 273);
            this.groupBox9.TabIndex = 0;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "最大分辨率计算";
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.txtChipDiagonal2);
            this.groupBox10.Controls.Add(this.txtChipHeight2);
            this.groupBox10.Controls.Add(this.txtChipWidth2);
            this.groupBox10.Controls.Add(this.label26);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.label28);
            this.groupBox10.Controls.Add(this.cmbChipSize2);
            this.groupBox10.Location = new System.Drawing.Point(6, 20);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(158, 232);
            this.groupBox10.TabIndex = 13;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "芯片尺寸";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(11, 204);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(137, 12);
            this.label25.TabIndex = 10;
            this.label25.Text = "芯片尺寸不对请手动改正";
            // 
            // txtChipDiagonal2
            // 
            this.txtChipDiagonal2.Location = new System.Drawing.Point(20, 172);
            this.txtChipDiagonal2.Name = "txtChipDiagonal2";
            this.txtChipDiagonal2.ReadOnly = true;
            this.txtChipDiagonal2.Size = new System.Drawing.Size(121, 21);
            this.txtChipDiagonal2.TabIndex = 7;
            // 
            // txtChipHeight2
            // 
            this.txtChipHeight2.Location = new System.Drawing.Point(20, 128);
            this.txtChipHeight2.Name = "txtChipHeight2";
            this.txtChipHeight2.Size = new System.Drawing.Size(121, 21);
            this.txtChipHeight2.TabIndex = 8;
            // 
            // txtChipWidth2
            // 
            this.txtChipWidth2.Location = new System.Drawing.Point(20, 84);
            this.txtChipWidth2.Name = "txtChipWidth2";
            this.txtChipWidth2.Size = new System.Drawing.Size(121, 21);
            this.txtChipWidth2.TabIndex = 9;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(18, 157);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(77, 12);
            this.label26.TabIndex = 4;
            this.label26.Text = "对角线(mm)：";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(18, 113);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(77, 12);
            this.label27.TabIndex = 5;
            this.label27.Text = "芯片高(mm)：";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(18, 67);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(77, 12);
            this.label28.TabIndex = 6;
            this.label28.Text = "芯片宽(mm)：";
            // 
            // cmbChipSize2
            // 
            this.cmbChipSize2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbChipSize2.FormattingEnabled = true;
            this.cmbChipSize2.Items.AddRange(new object[] {
            "1/4英寸",
            "1/3英寸",
            "1/2.5英寸",
            "1/2.3英寸",
            "1/2英寸",
            "1/1.8英寸",
            "2/3英寸",
            "1英寸",
            "1.1英寸",
            "35mm film"});
            this.cmbChipSize2.Location = new System.Drawing.Point(20, 39);
            this.cmbChipSize2.Name = "cmbChipSize2";
            this.cmbChipSize2.Size = new System.Drawing.Size(121, 20);
            this.cmbChipSize2.TabIndex = 3;
            this.cmbChipSize2.SelectedIndexChanged += new System.EventHandler(this.cmbChipSize2_SelectedIndexChanged);
            // 
            // txtMaxResolution
            // 
            this.txtMaxResolution.Location = new System.Drawing.Point(177, 192);
            this.txtMaxResolution.Name = "txtMaxResolution";
            this.txtMaxResolution.Size = new System.Drawing.Size(121, 21);
            this.txtMaxResolution.TabIndex = 10;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(175, 170);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(113, 12);
            this.label21.TabIndex = 0;
            this.label21.Text = "可支持最大分辨率：";
            // 
            // txtMTF
            // 
            this.txtMTF.Location = new System.Drawing.Point(177, 74);
            this.txtMTF.Name = "txtMTF";
            this.txtMTF.Size = new System.Drawing.Size(121, 21);
            this.txtMTF.TabIndex = 10;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(175, 59);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(113, 12);
            this.label24.TabIndex = 0;
            this.label24.Text = "镜头MTF（lp/mm）：";
            // 
            // btnCompute3
            // 
            this.btnCompute3.Location = new System.Drawing.Point(199, 133);
            this.btnCompute3.Name = "btnCompute3";
            this.btnCompute3.Size = new System.Drawing.Size(75, 23);
            this.btnCompute3.TabIndex = 11;
            this.btnCompute3.Text = "计算";
            this.btnCompute3.UseVisualStyleBackColor = true;
            this.btnCompute3.Click += new System.EventHandler(this.btnCompute3_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.label20);
            this.groupBox8.Controls.Add(this.btnCompute2);
            this.groupBox8.Controls.Add(this.txtMagnification);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this.txtChipLength);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.txtObjectLength);
            this.groupBox8.Controls.Add(this.label17);
            this.groupBox8.Location = new System.Drawing.Point(8, 12);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(173, 209);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "倍率计算";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(17, 151);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 24);
            this.label20.TabIndex = 12;
            this.label20.Text = "以上任意填写两项后计算\r\n第三项";
            // 
            // btnCompute2
            // 
            this.btnCompute2.Location = new System.Drawing.Point(41, 180);
            this.btnCompute2.Name = "btnCompute2";
            this.btnCompute2.Size = new System.Drawing.Size(75, 23);
            this.btnCompute2.TabIndex = 11;
            this.btnCompute2.Text = "计算";
            this.btnCompute2.UseVisualStyleBackColor = true;
            this.btnCompute2.Click += new System.EventHandler(this.btnCompute2_Click);
            // 
            // txtMagnification
            // 
            this.txtMagnification.Location = new System.Drawing.Point(19, 119);
            this.txtMagnification.Name = "txtMagnification";
            this.txtMagnification.Size = new System.Drawing.Size(121, 21);
            this.txtMagnification.TabIndex = 10;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(17, 104);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 12);
            this.label19.TabIndex = 0;
            this.label19.Text = "光学放大倍率：";
            // 
            // txtChipLength
            // 
            this.txtChipLength.Location = new System.Drawing.Point(19, 80);
            this.txtChipLength.Name = "txtChipLength";
            this.txtChipLength.Size = new System.Drawing.Size(121, 21);
            this.txtChipLength.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(17, 65);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(149, 12);
            this.label18.TabIndex = 0;
            this.label18.Text = "芯片对应单边长度（mm）：";
            // 
            // txtObjectLength
            // 
            this.txtObjectLength.Location = new System.Drawing.Point(19, 41);
            this.txtObjectLength.Name = "txtObjectLength";
            this.txtObjectLength.Size = new System.Drawing.Size(121, 21);
            this.txtObjectLength.TabIndex = 10;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(17, 26);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(125, 12);
            this.label17.TabIndex = 0;
            this.label17.Text = "物方单边长度（mm）：";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Image = global::OiTool.Properties.Resources.OiPictrue;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(536, 64);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 505);
            this.Controls.Add(this.tabControl2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "镜头选型助手";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabControl2.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslCompany;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtChipHeight;
        private System.Windows.Forms.TextBox txtChipWidth;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbChipSize;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtObjectDistance;
        private System.Windows.Forms.TextBox txtObjectHeight;
        private System.Windows.Forms.TextBox txtObjectWidth;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtFocalDistance;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btnCompute;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnCompute1;
        private System.Windows.Forms.TextBox txtObjectWidth1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtObjectHeight1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnClear1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtFocalDistance1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtObjectDistance1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox txtChipDiagonal;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtChipDiagonal1;
        private System.Windows.Forms.TextBox txtChipHeight1;
        private System.Windows.Forms.TextBox txtChipWidth1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbChipSize1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtProcess;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btnCompute2;
        private System.Windows.Forms.TextBox txtMagnification;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtChipLength;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtObjectLength;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtChipDiagonal2;
        private System.Windows.Forms.TextBox txtChipHeight2;
        private System.Windows.Forms.TextBox txtChipWidth2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cmbChipSize2;
        private System.Windows.Forms.TextBox txtMaxResolution;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtMTF;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Button btnCompute3;
    }
}

