﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OiTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SetChipSize(decimal width, decimal height, decimal diagonal)
        {
            txtChipHeight.Text = height.ToString("0.00");
            txtChipWidth.Text = width.ToString("0.00");
            txtChipDiagonal.Text = diagonal.ToString("0.00");
        }

        private void SetChipSize(decimal inch)
        {
            txtChipHeight.Text = (9.6M * inch).ToString("0.0");
            txtChipWidth.Text = (12.8M * inch).ToString("0.0");
            txtChipDiagonal.Text = (16M * inch).ToString("0.0");
        }

        private void SetChipSize1(decimal width, decimal height, decimal diagonal)
        {
            txtChipHeight1.Text = height.ToString("0.00");
            txtChipWidth1.Text = width.ToString("0.00");
            txtChipDiagonal1.Text = diagonal.ToString("0.00");
        }

        private void SetChipSize1(decimal inch)
        {
            txtChipHeight1.Text = (9.6M * inch).ToString("0.0");
            txtChipWidth1.Text = (12.8M * inch).ToString("0.0");
            txtChipDiagonal1.Text = (16M * inch).ToString("0.0");
        }

        private void SetChipSize2(decimal width, decimal height, decimal diagonal)
        {
            txtChipHeight2.Text = height.ToString("0.00");
            txtChipWidth2.Text = width.ToString("0.00");
            txtChipDiagonal2.Text = diagonal.ToString("0.00");
        }

        private void SetChipSize2(decimal inch)
        {
            txtChipHeight2.Text = (9.6M * inch).ToString("0.0");
            txtChipWidth2.Text = (12.8M * inch).ToString("0.0");
            txtChipDiagonal2.Text = (16M * inch).ToString("0.0");
        }

        private bool CheckContinue(out decimal objectWidth, out decimal objectHeight, out decimal objectDistance)
        {
            objectWidth = 0;
            objectHeight = 0;
            objectDistance = 0;

            if (cmbChipSize.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择芯片");
                return false;
            }

            if (!decimal.TryParse(txtObjectWidth.Text, out objectWidth))
            {
                MessageBox.Show("物宽输入有误");
                return false;
            }

            if (!decimal.TryParse(txtObjectHeight.Text, out objectHeight))
            {
                MessageBox.Show("物高输入有误");
                return false;
            }

            if (!decimal.TryParse(txtObjectDistance.Text, out objectDistance))
            {
                MessageBox.Show("物距输入有误");
                return false;
            }

            return true;
        }

        private bool CheckContinue1(out decimal focalDistance, out decimal objectDistance)
        {
            focalDistance = 0;
            objectDistance = 0;

            if (cmbChipSize1.SelectedIndex == -1)
            {
                MessageBox.Show("请先选择芯片");
                return false;
            }

            if (!decimal.TryParse(txtFocalDistance1.Text, out focalDistance))
            {
                MessageBox.Show("焦距输入有误");
                return false;
            }

            if (!decimal.TryParse(txtObjectDistance1.Text, out objectDistance))
            {
                MessageBox.Show("物宽输入有误");
                return false;
            }

            return true;
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void tsslCompany_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.oi-smart.com/");
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtObjectDistance.Clear();
            txtObjectHeight.Clear();
            txtObjectWidth.Clear();
        }

        private void cmbChipSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = cmbChipSize.SelectedItem as string;

            switch (selected)
            {
                case "1/4英寸":
                    SetChipSize(1M / 4);
                    break;
                case "1/3英寸":
                    SetChipSize(4.8m, 3.6m, 6m);
                    break;
                case "1/2.5英寸":
                    SetChipSize(5.76m, 4.29m, 7.18m);
                    break;
                case "1/2.3英寸":
                    SetChipSize(6.16m, 4.62m, 7.7m);
                    break;
                case "1/2英寸":
                    SetChipSize(1M / 2);
                    break;
                case "1/1.8英寸":
                    SetChipSize(7.18m, 5.32m, 8.94m);
                    break;
                case "2/3英寸":
                    SetChipSize(8.8m, 6.6m, 11m);
                    break;
                case "1英寸":
                    SetChipSize(1);
                    break;
                case "1.1英寸":
                    SetChipSize(12m, 12m, 17m);
                    break;
                case "35mm film":
                    SetChipSize(36m, 24m, 43.27m);
                    break;
                default:
                    return;
            }
        }

        private void cmbChipSize1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = cmbChipSize1.SelectedItem as string;

            switch (selected)
            {
                case "1/4英寸":
                    SetChipSize1(1M / 4);
                    break;
                case "1/3英寸":
                    SetChipSize1(4.8m, 3.6m, 6m);
                    break;
                case "1/2.5英寸":
                    SetChipSize1(5.76m, 4.29m, 7.18m);
                    break;
                case "1/2.3英寸":
                    SetChipSize1(6.16m, 4.62m, 7.7m);
                    break;
                case "1/2英寸":
                    SetChipSize1(1M / 2);
                    break;
                case "1/1.8英寸":
                    SetChipSize1(7.18m, 5.32m, 8.94m);
                    break;
                case "2/3英寸":
                    SetChipSize1(8.8m, 6.6m, 11m);
                    break;
                case "1英寸":
                    SetChipSize1(1);
                    break;
                case "1.1英寸":
                    SetChipSize1(12m, 12m, 17m);
                    break;
                case "35mm film":
                    SetChipSize1(36m, 24m, 43.27m);
                    break;
                default:
                    return;
            }
        }

        private void btnCompute_Click(object sender, EventArgs e)
        {
            decimal objectWidth;
            decimal objectHeight;
            decimal objectDistance;

            if (!CheckContinue(out objectWidth, out objectHeight, out objectDistance))
                return;

            decimal chipWidth = decimal.Parse(txtChipWidth.Text);
            decimal chipHeight = decimal.Parse(txtChipHeight.Text);

            txtProcess.Clear();
            txtProcess.AppendText(string.Format("芯片宽度v={0}mm,高度h={1}mm,物距D={2}mm,被摄物体宽度V={3}mm,高度H={4}mm\r\n", chipWidth, chipHeight, objectDistance, objectWidth, objectHeight));
            decimal f1 = chipWidth * objectDistance / objectWidth;
            txtProcess.AppendText(string.Format("焦距f1=v*D/V={0}*{1}/{2}={3}\r\n", chipWidth, objectDistance, objectWidth, f1));
            decimal f2 = chipHeight * objectDistance / objectHeight;
            txtProcess.AppendText(string.Format("焦距f2=h*D/H={0}*{1}/{2}={3}\r\n", chipHeight, objectDistance, objectHeight, f2));
            txtProcess.AppendText(string.Format("最终选择较小的焦距：{0}", f1 > f2 ? f2 : f1));

            txtFocalDistance.Text = (f1 > f2 ? f2 : f1).ToString();
        }

        private void btnClear1_Click(object sender, EventArgs e)
        {
            txtObjectDistance1.Clear();
            txtFocalDistance1.Clear();
        }

        private void btnCompute1_Click(object sender, EventArgs e)
        {
            decimal focalDistance;
            decimal objectDistance;

            if (!CheckContinue1(out focalDistance, out objectDistance))
                return;

            decimal chipWidth = decimal.Parse(txtChipWidth1.Text);
            decimal chipHeight = decimal.Parse(txtChipHeight1.Text);

            txtObjectWidth1.Text = (chipWidth * objectDistance / focalDistance).ToString();
            txtObjectHeight1.Text = (chipHeight * objectDistance / focalDistance).ToString();
        }

        private void btnCompute2_Click(object sender, EventArgs e)
        {
            string a = txtObjectLength.Text.Trim();
            string b = txtChipLength.Text.Trim();
            string c = txtMagnification.Text.Trim();

            decimal objectLength, chipLength, magnification;

            bool isObjLength = decimal.TryParse(a, out objectLength);
            bool isChipLength = decimal.TryParse(b, out chipLength);
            bool isMagni = decimal.TryParse(c, out magnification);

            if (!isObjLength && isChipLength && isMagni)
            {
                objectLength = chipLength / magnification;
                txtObjectLength.Text = objectLength.ToString();
            }
            else if (isObjLength && !isChipLength && isMagni)
            {
                chipLength = objectLength * magnification;
                txtChipLength.Text = chipLength.ToString();
            }
            else if (isObjLength && isChipLength && !isMagni)
            {
                magnification = chipLength / objectLength;
                txtMagnification.Text = magnification.ToString();
            }
            else
            {
                MessageBox.Show("输入有误或者三者都有数据");
            }
        }

        private void cmbChipSize2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selected = cmbChipSize2.SelectedItem as string;

            switch (selected)
            {
                case "1/4英寸":
                    SetChipSize2(1M / 4);
                    break;
                case "1/3英寸":
                    SetChipSize2(4.8m, 3.6m, 6m);
                    break;
                case "1/2.5英寸":
                    SetChipSize2(5.76m, 4.29m, 7.18m);
                    break;
                case "1/2.3英寸":
                    SetChipSize2(6.16m, 4.62m, 7.7m);
                    break;
                case "1/2英寸":
                    SetChipSize2(1M / 2);
                    break;
                case "1/1.8英寸":
                    SetChipSize2(7.18m, 5.32m, 8.94m);
                    break;
                case "2/3英寸":
                    SetChipSize2(8.8m, 6.6m, 11m);
                    break;
                case "1英寸":
                    SetChipSize2(1);
                    break;
                case "1.1英寸":
                    SetChipSize2(12m, 12m, 17m);
                    break;
                case "35mm film":
                    SetChipSize2(36m, 24m, 43.27m);
                    break;
                default:
                    return;
            }
        }

        private void btnCompute3_Click(object sender, EventArgs e)
        {
            string mtfStr = txtMTF.Text;

            decimal mtf;

            if (decimal.TryParse(mtfStr, out mtf))
            {
                decimal width = Convert.ToDecimal(txtChipWidth2.Text);
                decimal height = Convert.ToDecimal(txtChipHeight2.Text);

                txtMaxResolution.Text = string.Format("{0} * {1}", width*mtf*2, height*mtf*2);
            }
        }
    }
}
/*
1/4英寸
1/3英寸
1/2.5英寸
1/2.3英寸
1/2英寸
1/1.8英寸
2/3英寸
1英寸
35mm film
*/

// c = b/a
